$(function() {
  // Populate using an object literal
  WTF.init({
     heading: [
       "Best guy ever:",
       "Everybody wants him:",
       "Need a new anime crush?",
       "When's the dakimakura coming out?",
       "Best boy:",
     ],

     response: [
       "Not my type.",
       "Give me some other hot guy.",
       "Sounds too similar to my ex.",
     ],

     template: [
       "@job who has @hair_style @color hair.",
       "Quiet boy with @hair_style @color hair who is !article @job.",
       "Ultraman.",
       "delicious @job sundae topped with @hair_style @color sprinkles.",
       "sexy beast of !article @job. Just look at him. Just look at his @hair_style @color mane.",
       "@job hearthrob who enthralls men and women alike with his @hair_style @color hair",
       "a pork cutlet bowl fatale of a @job that enthralls men",
       "distracted @job boy who needs a comb for his @color @hair_style hair",
       "beautiful man who is searching for his true love while being !article @job on the side.",
       "pretty @job with @hair_style @color hair and eyes for no one but you.",
     ],

     color: [
       "red",
       "black",
       "blond",
       "silver",
       "golden",
       "blue",
       "chestnut",
       "bronze",
       "white",
       "green",
       "orange",
       "maroon",
       "mauve",
       "crimson",
       "purple",
       
     ],

     hair_style: [
       "long flowy",
       "spiky",
       "natural",
       "long luscious",
       "short",
       "flowing locks of",
       "fluffy",
       "messy",
       "wind-blown",
       "shaggy",
       "soft",
       "luxurious",
       "tufty"
       
     ],

     job: [
       "world renowned figure skater",
       "costumed crime fighter",
       "mercenary band leader",
       "retainer to the princess",
       "introverted bartender",
       "theater student",
       "renegade wizard",
       "master swordsman",
       "prince of a host club",
       "emo bad boy",
       "indentured servant",
       "lonely indie game designer",
       "reclusive polymath and  a really good cook",
       "hot b-class hero",
       "emo ninja",
       "mech suit pilot",
       "violinist who can't get a date",
       "mad scientist",
       "kind senpai",
       "setter for a high school volleyball team",
       "team mom",
       "super hax0r",
       "small angsty cat boy",
       "muscular shark prince",
     ],
   });

  /**
  /* Populate using a JSON file
  /* WTF.init( 'sample.json' );
  /*
  /* Populate using a Google spreadsheet ID (you must publish it first!)
  /* @see https://support.google.com/drive/answer/37579?hl=en
  /* WTF.init( '0AvG1Hx204EyydF9ub1M2cVJ3Z1VGdDhTSWg0ZV9LNGc' );
   */
});
