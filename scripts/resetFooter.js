(function(window){
  // Gets footer, there's only one on the page
  var footer = $("footer")[0];
  var footerHeight = $(footer).height();

  var topMargin = 70;

  // Only applies to mobile phones with a specific width
  var dimensions = {
    width: $(window).width(),
    height: $(window).height()
  }

  /**
  /* If main content is too long, make footer follow
  /* the main content instead of staying fixed
   */
  function resetFooter() {
    if (dimensions.width < 570 && dimensions.width > 330) {
      console.log($("#getHeight").height());
      console.log($(footer).height());

      if ($("#getHeight").height() + footerHeight + topMargin >= dimensions.height) {
        $(footer).css({
            "position": "static"
        })
      } else {
        $(footer).css({
          "position": "absolute"
        })
      }
    }
  }

  window.resetFooter = resetFooter;
}(window))
